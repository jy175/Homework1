import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class Containers {
	
	public static void main (String[] args) {
		
		System.out.println("This is a program to test Containers\n");
		int[] arr = new int[7];
		arr[0] = 1; arr[1] = 1;
		arr[2] = 2; arr[3] = 2;
		arr[4] = 3; arr[5] = 3;
		arr[6] = 4;
		System.out.println("###################");
		System.out.println("The original array is as followed: ");
		for (int i=0; i<arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println("\n");
		
		List<Integer> l = new ArrayList<Integer>();
		Set<Integer> s = new HashSet<Integer>();
		Map<Integer, Integer> m = new HashMap<Integer, Integer>();
		
		for (int i=0; i<arr.length; i++) {
			l.add(arr[i]);
			s.add(arr[i]);
			m.put(arr[i], i);
		}
		
		// Print List
		System.out.println("###################");
		System.out.println("The LIST is as followed: ");
		for (int i: l) {
			System.out.print(i + " ");
		}
		System.out.println("\n");

		// Print Set
		System.out.println("###################");
		System.out.println("The SET is as followed: ");
		for (int i: s) {
			System.out.print(i + " ");
		}
		System.out.println("\n");
		
		// Print Map
		System.out.println("###################");
		System.out.println("The MAP is as followed: ");
		for (int i: m.keySet()) {
			System.out.print("(" + i + "," + m.get(i) + ") ");
		}
		System.out.println("\n");

		return;
	}
}
