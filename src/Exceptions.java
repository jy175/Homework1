public class Exceptions {
	
	public static void main(String[] args) {
		
		int[] dvd = new int[4];
		int[] dvs = new int[4];
		dvd[0] = 16; dvs[0] = 2;
		dvd[1] = 15; dvs[1] = 0;
		dvd[2] = Integer.MIN_VALUE; dvs[2] = -1;
		dvd[3] = 25; dvs[3] = 26;
	
		for (int i=0; i<dvd.length; i++) {
			try {
				System.out.println("####################################");
				int ans = divide(dvd[i], dvs[i]);
				System.out.println("This is a valid call for function divide!");
				System.out.println("The answer is " + ans);
				System.out.println();
			}
			catch(Exception e) {
				System.out.println("EXCEPTION detected in function divide!");
				System.out.println(e.getMessage());
				// System.err.println(e.getMessage());
				// standard error output might mess up with the order because it doesn't have a buffer
				System.out.println();
			}
		}
		
		return;
	}
	
	public static int divide(int dividend, int divisor) throws ArithmeticException {
		if (divisor == 0) {
			throw new ArithmeticException("Cannot divide ZERO!");
		}
		else if (dividend == Integer.MIN_VALUE && divisor == -1) {
			throw new ArithmeticException("The answer is too BIG for an INT!");
		}
		else {
			return dividend / divisor;
		}
	}
}