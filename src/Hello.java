
public class Hello {
	private static String s0 = "Default static string";
	
	public static void main(String[] args) {
		System.out.println("### Jiasheng Yang's Homework1! ###");
		System.out.println("Calling function PRINTLN from class Hello");
		System.out.println(s0);
		printFromStaticFunction();
		Hello h1 = new Hello();
		System.out.println("Now I've created a new object in this class");
		h1.printFromFunction();
		return;
	}
	
	private void printFromFunction() {
		System.out.println("This is a sentence printed from a non-static function!");
	}
	
	private static void printFromStaticFunction() {
		System.out.println("This is a sentence printed from a static function!");
	}
}
