import transportations.Transportation;
import transportations.Plane;
import transportations.Bicycle;
import transportations.Car;

public class Objects {
	public static void main(String[] args) {
		System.out.println("This is class Objects\n");
		// POLYMORPHISM
		Transportation[] t = new Transportation[3];
		t[0] = new Plane(500, true);
		t[1] = new Bicycle(10, false);
		t[2] = new Car(100, true);
		for (int i = 0; i < t.length; i++) {
			t[i].introduce();
			System.out.println();
			t[i].crashIntroduce();
			System.out.println();
		}
		System.out.println();
		Bicycle b = new Bicycle(8, false);
		b.betterHealth();

		return;
	}
}
