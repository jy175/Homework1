package transportations;

public class Bicycle extends Transportation implements Healthier {
	
	public Bicycle (int s, boolean mP) {
		super(s, mP, "BICYCLE");
	}
	
	public void crashIntroduce() {
		System.out.println("### CRASH INFO ###");
		System.out.println("If you crashed in a bicycle, you would probably get a broken leg");
		System.out.println("But luckily, nothing more");
		return;
	}
	
	public void betterHealth() {
		System.out.println("Cycling is also a good exercise. It's very good for your health.");
	}
}
