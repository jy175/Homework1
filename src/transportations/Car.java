package transportations;

public class Car extends Transportation {
	
	public Car(int s, boolean mP) {
		super(s, mP, "CAR");
	}
	
	public void crashIntroduce() {
		System.out.println("### CRASH INFO ###");
		System.out.println("That depends");
		System.out.println("In a minor accident, you go to see the insurance company");
		System.out.println("In a major accident, you go to see the god");
	}
}
