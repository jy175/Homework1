package transportations;

public class Plane extends Transportation {
	
	public Plane(int s, boolean mP) {
		super(s, mP, "AIR PLANE");
	}
	
	public void crashIntroduce() {
		System.out.println("### CRASH INFO ###");
		System.out.println("If a plane crashed, there isn't much hope for any passenger to live.");
		System.out.println("But don't worry, the odd that a plane will crash is suprisingly low");
		return;
	}
}
