package transportations;

public abstract class Transportation {
	protected int speed;
	protected boolean multiPeople;
	protected String name;
	
	public Transportation(int s, Boolean mP, String n) {
		this.speed = s;
		this.multiPeople = mP;
		this.name = n;
	}
	
	public void introduce() {
		System.out.println("### Transportation: " + name +" ###");
		System.out.println("Speed: " + speed);
		if (multiPeople) {
			System.out.println("Can take multiple people at the same time");
		}
		else {
			System.out.println("Could only take one person at a time");
		}
		return;
	}
	
	public abstract void crashIntroduce();
}
